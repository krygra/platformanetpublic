﻿using ProjectNet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectNet.Controllers
{
    public class SelectBooksController : Controller
    {
        // GET: SelectBooks

        datamvcDataContext db = new datamvcDataContext();

        public ActionResult Index()
        {
            List<Book> model = db.Books.ToList();

            return View(model);
        }

        public ActionResult OrderByTitle()
        {
            var book = from p in db.Books
                       orderby p.Tytuł ascending
                       select p;
            return View(book);
        }

        public ActionResult OrderByDate()
        {
            var book = from p in db.Books
                       orderby p.Data_wydania ascending
                       select p;
            return View(book);
        }
    }
}
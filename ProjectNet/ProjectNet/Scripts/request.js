﻿$(document).ready(function () {

    $("#sender").click(function () {

        var url = '';
        var img = '';
        var title = '';
        var author = '';
        var btn = '';
        var search = $("#books").val();

        if (search == '') {
            alert("Wpisz frazę według której chcesz szukać książki.");
        }
        else {

            with (document.getElementById('result')) while (hasChildNodes()) removeChild(firstChild)

            $.get("https://www.googleapis.com/books/v1/volumes?q=" + search, function (response) {

                for (i = 0; i < response.items.length; i++) {
                    //get title of the book
                    title = $('<h5 class="center-align white-text">' + response.items[i].volumeInfo.title + '</h5>');
                    //get author of the book
                    author = $('<h5 class="center-align white-text">' + response.items[i].volumeInfo.authors + '</h5>');
                    //get image of the book
                    img = $('<img class="aligning card z-depth-5" id="dynamic"><br><a href=' + response.items[i].volumeInfo.infoLink + '><button id="imagebutton" class="btn red aligning">Zobacz</button></a>');
                    //get url of the book
                    //Dodać try catch
                    url = response.items[i].volumeInfo.imageLinks.thumbnail;

                    //btn = $('<button id="addBookBtn" onclick="addBook()" class="btn red aligning">Dodaj do bazy</button>');

                    img.attr('src', url); //attach the image url

                    title.appendTo("#result");

                    author.appendTo("#result");

                    img.appendTo("#result");

                    //btn.appendTo("#result");
                }

            });
        }

    });



});

function addBook() {
    alert("Dodano książkę do bazy");

    var title = '';
    var author = '';
    var description = '';
    var releaseDate = '';


}